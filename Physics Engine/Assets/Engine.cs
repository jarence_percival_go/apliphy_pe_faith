﻿using UnityEngine;
using System.Collections;

public class Engine : MonoBehaviour {
    public GameObject Cube;
    public GameObject Player;
    Vector2 cubemin, cubemax;
    Vector2 player1min, player1max;
    float mass = 1f;
    float acceleration;
    float velocity;
    struct AABB
    {
        Vector2 cubemin, cubemax;
        Vector2 player1min, player1max;
    };

 
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    // Two crossed vectors return a scalar
    void FixUpdate()
    {
        //Cube
        float A1x = Cube.transform.position.x + Cube.transform.localScale.x / 2;
        float A2x = Cube.transform.position.x - Cube.transform.localScale.x / 2;
        float A1y = Cube.transform.position.x + Cube.transform.localScale.x / 2;
        float A2y = Cube.transform.position.y - Cube.transform.localScale.x / 2;
        cubemax = new Vector2(A1x, A1y);
        cubemin = new Vector2(A2x, A2y);
        //player
        float B1x = Player.transform.position.x + Player.transform.localScale.x / 2;
        float B2x = Player.transform.position.x - Player.transform.localScale.x / 2;
        float B1y = Player.transform.position.x + Player.transform.localScale.x / 2;
        float B2y = Player.transform.position.y - Player.transform.localScale.x / 2;
        player1max = new Vector2(B1x, B1y);
        player1min = new Vector2(B2x, B2y);


    }
    bool AABBvsAABB(AABB a, AABB b)
    {
        // Exit with no intersection if found separated along an axis
        if (cubemax.x < player1min.x || cubemin.x > player1max.x) return false;
        if (cubemax.y < player1min.y || cubemin.y > player1max.y) return false;

        return true;
    }

    void ResolveCollision(GameObject Cube, GameObject Player)
    {
        // Calculate relative velocity
        Vector2 rv = Cube.velocity- Player.velocity;

    // Calculate relative velocity in terms of the normal direction
        float velAlongNormal = DotProduct(rv, normal)

    // Do not resolve if velocities are separating
        if (velAlongNormal > 0)
        return;

     // Calculate restitution
        float e = min(A.restitution, B.restitution)

    // Calculate impulse scalar
        float j = -(1 + e) * velAlongNormal
      j /= 1 / A.mass + 1 / B.mass;

      // Apply impulse
        Vector2 impulse = j * normal;
        Cube.velocity -= 1 / Cube.mass * impulse;
        Player.velocity += 1 / Player.mass * impulse;
    }
    Cube.inv_mass = 1 / Cube.mass

    float mass_sum = Cube.mass + Player.mass;
    float ratio = Cube.mass / mass_sum;
    Cube.velocity -= ratio* impulse;

    ratio = Player.mass / mass_sum;
    Player.velocity += ratio* impulse;


    void PositionalCorrection(GameObject Cube, GameObject Player)
    {
     const float percent = 0.2f; 
     Vector2 correction = penetrationDepth / (Cube.inv_mass + Player.inv_mass)) *percent * n;
     Cube.transform.position -= Cube.inv_mass * correction
     Player.tranformposition += Player.inv_mass * correction
}

}
