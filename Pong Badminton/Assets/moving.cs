﻿using UnityEngine;
using System.Collections;

public class moving : MonoBehaviour {

    public GameObject ball;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject Cube2;
    public GameObject Cube3;
    Vector2 ballmin,ballmax;
    Vector2 player1min, player1max;
    Vector2 player2min, player2max;
    Vector2 c2min, c2max;
    Vector2 c3min, c3max;
    bool LeftHit = false;
    bool RightHit = false;
    bool UpHit = false;
    bool DownHit = false;
    float acceleration = 0.0f;
    float velocity = 0.01f;
    float gravity = 0.5f;
    
    // Use this for initialization
    void Start () {
         
    }

    // Update is called once per frame
    void Update()
    {
        velocity += acceleration;
        transform.position += new Vector3(velocity, 0.0f, 0.0f);

        FixedUpdate();
    }
    bool Detectcollision()
    {


        return false;

    }
    void FixedUpdate()
    {
        //Ball 
        float gravity = 0.5f;
        float A1x = ball.transform.position.x + ball.transform.localScale.x / 2;
        float A2x = ball.transform.position.x - ball.transform.localScale.x / 2;
        float A1y = ball.transform.position.x + ball.transform.localScale.x / 2;
        float A2y = ball.transform.position.y - ball.transform.localScale.x / 2;

        ballmax = new Vector2(A1x, A1y);
        ballmin = new Vector2(A2x, A2y);
        //Player 1
        float B1x = Player1.transform.position.x + Player1.transform.localScale.x / 2;
        float B2x = Player1.transform.position.x - Player1.transform.localScale.x / 2;
        float B1y = Player1.transform.position.x + Player1.transform.localScale.x / 2;
        float B2y = Player1.transform.position.y - Player1.transform.localScale.x / 2;

        player1max = new Vector2(B1x, B1y);
        player1min = new Vector2(B2x, B2y);
        //Player 2
        float C1x = Player2.transform.position.x + Player2.transform.localScale.x / 2;
        float C2x = Player2.transform.position.x - Player2.transform.localScale.x / 2;
        float C1y = Player2.transform.position.x + Player2.transform.localScale.x / 2;
        float C2y = Player2.transform.position.y - Player2.transform.localScale.x / 2;

        player2max = new Vector2(C1x, C1y);
        player2min = new Vector2(C2x, C2y);

        // Wall 2
        float D1x = Cube2.transform.position.x + Cube2.transform.localScale.x / 2;
        float D2x = Cube2.transform.position.x - Cube2.transform.localScale.x / 2;
        float D1y = Cube2.transform.position.x + Cube2.transform.localScale.x / 2;
        float D2y = Cube2.transform.position.y - Cube2.transform.localScale.x / 2;

        c2max = new Vector2(D1x, D1y);
        c2min = new Vector2(D2x, D2y);

        //Wall 3
        float E1x = Cube3.transform.position.x + Cube3.transform.localScale.x / 2;
        float E2x = Cube3.transform.position.x - Cube3.transform.localScale.x / 2;
        float E1y = Cube3.transform.position.x + Cube3.transform.localScale.x / 2;
        float E2y = Cube3.transform.position.y - Cube3.transform.localScale.x / 2;

        c3max = new Vector2(E1x, E1y);
        c3min = new Vector2(E2x, E2y);

        A1x += 0.03f;
        A2x += 0.03f;
        A1y += 0.03f;
        A2y += 0.03f;

        velocity += acceleration ;
        transform.position += new Vector3(velocity, -gravity, 0.0f);
        //if ((A1x <= B2x && A2x >= B1x) && (A1y >= B2y && A2y <= B2y))
        //{
        //    LeftHit = true;
        //}
        //if ((A2x >= C1x && A1x <= C2x) && (A2y <= C1y && A1y >= C2y))
        //{
        //    LeftHit = false;
        //}
        //if ((A1x <= D2x && A2x >= D1x) && (A1y >= D2y && A2y <= D2y))
        //{
        //    UpHit = true;
        //}
        //if ((A2y <= E1y && A2y >= E2y) && (A2x >= E1x && A1x <= E2x))
        //{
        //    UpHit = false;
        //}
        //if ((A1x <= B2x && A2x >= B1x) && (A1y >= B2y && A2y <= B2y))
        //{
        //    DownHit = true;
        //}
        //if ((A2y <= B1y && A2y >= B2y) && (A2x >= B1x && A1x <= B2x))
        //{
        //    DownHit = false;
        //}
        //if ((A2x >= C1x && A1x <= C2x) && (A1y <= C1y && A2y >= C2y))
        //{
        //    RightHit = true;
        //}

        //if ((A2x <= B1x && A2x >= B2x) && (A2y >= B1y && A1y <= B2y))
        //{
        //    RightHit = false;
        //}
        //if (LeftHit)
        //{
        //    A1x += 0.025f;
        //    A2x += 0.025f;
        //    A1x += 0.010f;
        //    A2x += 0.010f;
        //}
        //else
        //{
        //    A1x -= 0.035f;
        //    A2x -= 0.035f;
        //}


        //if (UpHit)
        //{

        //    A1x += 0.025f;
        //    A2x += 0.025f;

        //    A1y -= 0.018f;
        //    A2y -= 0.018f;
        //}
        //if (DownHit)
        //{
        //    A1x -= 0.025f;
        //    A2x -= 0.025f;

        //    A1y += 0.025f;
        //    A2y += 0.025f;
        //}

    }

    
    //bool detectCollision()
    //{

    //    if (transform.position.x < end)
    //    {
    //        velocity = new Vector2(0.0f, 0.0f);
    //        transform.position -= velocity + Dir;
    //        return true;
    //    }
    //    if(transform.position.x > end2)
    //    {
    //        velocity = new Vector2(0.0f, 0.0f);
    //        transform.position += velocity - Dirx;
    //        return true;
    //    }
    //    return false;
    //}

    //void move(int dir)
    //{
    //    if (dir == LEFT)
    //    {
    //        transform.position -= velocity + Dir;

    //    }
    //    else if (dir == RIGHT)
    //    {
    //        transform.position += velocity - Dirx;
    //        dir = LEFT;
    //    }
    //}
    ///* bool testaabboverlap(aabb*a,aabb*b)
    //{
    //    float d1x = b->min.x - a->max.x;
    //    float d1y = b->min.y - a->max.y;
    //    float d2x = a->min.x - b->max.x;
    //    float d2y = a->min.x - b->max.y;

    //    if (d1x > 0.0f || d1y > 0.0f) return false;

    //    if (d2x > 0.0f || d2y > 0.0f) return false;

    //    return true;
    //} */

}
