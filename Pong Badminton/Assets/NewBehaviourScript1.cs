﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.position.x < 9.62)
                transform.position += new Vector3(0.3f, 0.0f, 0.0f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (transform.position.x > 0.02)
                transform.position += new Vector3(-0.3f, 0.0f, 0.0f);
        }
    }
}
